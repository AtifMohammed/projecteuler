import java.io.BufferedReader;
import java.io.FileReader;


public class Solution {
//array size depending on the size of the triangle
	long input[][]=new long[100][100];
	long max;
	public void run(){
		int i=0;
		try{
			//reading input from external file
			BufferedReader br=new BufferedReader(new FileReader("triangle.txt"));
			String str;
			i=0;
			//reads whole file line by line
			while((str=br.readLine())!=null){
				max=0;
				String[] in=str.split(" ");
				for(int j=0; j<in.length; j++){
						long buf=Long.parseLong(in[j]);
						if(i==0){
							input[i][j]=buf;
						//for first digit adds only the value to its top-right
						}else if(j==0){
							input[i][j]=input[i-1][j]+buf;
							if(max<input[i][j]) max=input[i][j];
						//for last digit adds only the value on its top-left
						}else if(j==i){
							input[i][j]=input[i-1][j-1]+buf;
							if(max<input[i][j]) max=input[i][j];
						//checks for the lest value from its top-left and top-right
						}else{
							if(input[i-1][j-1]>input[i-1][j]){
								input[i][j]=input[i-1][j-1]+buf;
								if(max<input[i][j]) max=input[i][j];
							}else{
								input[i][j]=input[i-1][j]+buf;
								if(max<input[i][j]) max=input[i][j];
							}
						}
				}
				i++;
				//prints the max value till each row
				System.out.println("Max value till row "+i+" is "+max);
			}
			
		
		}catch(Exception e){
			System.out.print(e);
		}
	}
}
